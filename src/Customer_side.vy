# Smart Forward contract

tstart: public(uint256)
tend: public(uint256)
owner: public(address)
energyblock: public(int128)
price_electricity: public(int128)
total: public(int128)
power: public(int128[96])

# Dynamic array of type uint256, max 3 elements
nums: DynArray[uint256, 3]

event Deposit:
    sender: indexed(address)
    amount: int128
 
event printDynArray:
  value: DynArray[uint256, 1024]

@external
def __init__(_tstart: uint256,
             _tend: uint256,
             _energy: int128,
             _price: int128,
             ):
             
             
    self.owner = msg.sender

    self.tstart = _tstart  
    self.tend = self.tstart + _tend
    self.energyblock = _energy
    self.price_electricity = _price

@external
def negotiate() -> int128:
    self.total= self.energyblock*self.price_electricity
    
    log Deposit(self.owner, self.energyblock)
    return self.total
