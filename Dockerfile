FROM --platform=linux/amd64 python:3.8 AS compile-image
RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y --no-install-recommends build-essential gcc git

RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt


FROM --platform=linux/amd64 python:3.8 AS build-image
ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip
COPY --from=compile-image /root/.cache /root/.cache
COPY requirements.txt .
RUN pip install -r requirements.txt

WORKDIR /root
# COPY ./src/contract.sol .
COPY ./src .
# RUN vyper Customer_side.vy
# RUN vyper -f bytecode Customer_side.vy > Customer_side.bin
# RUN vyper -f abi Customer_side.vy > Customer_side.abi



RUN chmod +x ./app.py
CMD ["bash", "./app.py"] 


